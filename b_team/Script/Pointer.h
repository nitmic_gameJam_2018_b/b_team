#pragma once
#include "Field.h"


class Pointer {
private:
	Point pointingPos;
	Field& field;
	int t; //�`��p

public:
	Pointer(Field& field);
	~Pointer() = default;
	void update();
	void draw() const;

};