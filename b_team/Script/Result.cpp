#include "Result.h"

void Result::init()
{
	SoundAsset(L"drum").play();
	delta = (float)m_data->score / (SoundAsset(L"drum").lengthSec()*60);
	gameFont = Font(40, L"coopoppo 3D");
	font = Font(23);
}

void Result::update()
{
	if (drumming) {

		if ((score_ += delta) > m_data->score) score_ = m_data->score;

		if (!drum_fin && !SoundAsset(L"drum").isPlaying()) {
			SoundAsset(L"drum_fin").play();
			drum_fin = true;
		}
		else if (drum_fin && !SoundAsset(L"drum_fin").isPlaying()) {
			SoundAsset(L"cheer").play();
			drumming = false;
		}
	}
	else {
		if (++t / 8 > 5) t = 0;
		if (Input::KeyJ.clicked) changeScene(L"Title");
	}
}

void Result::draw() const
{
	if (drumming) TextureAsset(L"result")(0, 0, 640, 480).draw();
	else TextureAsset(L"result")(((t / 8) % 3) * 640, ((t / 8) / 3) * 480, 640, 480).draw();

	gameFont(L"SCORE\n", (int)score_).draw(130, 120, Palette::Black);
	if (!drumming) font(L"Jでタイトルに戻る").drawAt(Window::Center().movedBy(0, Window::Height()/2-65), Palette::Black);

}