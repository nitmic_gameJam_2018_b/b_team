# include <Siv3D.hpp>
# define rep(n) for (int i = 0; i < n; i++)


constexpr size_t colLen = 10;
constexpr size_t rowLen = 7;
constexpr size_t pointerSize = 32;
constexpr size_t cellSize = 64;
constexpr size_t offset_x = cellSize / 2;
constexpr size_t offset_y = cellSize;

constexpr size_t num_of_selectPin = 4;

constexpr size_t monster_capacity = 60;

constexpr int damageCoeff = 80;