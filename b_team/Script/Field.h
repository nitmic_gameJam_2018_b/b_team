#pragma once
#include "ConcreteMonster.h"


class MonsterGenerator {
private:
	std::list<std::shared_ptr<Monster>>& monsterList;
	const int interval;
	int increase_coeff;
	int t;
	int count;

public:
	MonsterGenerator(std::list<std::shared_ptr<Monster>>& _);
	~MonsterGenerator() = default;
	void generate();
};


class Field {
private:
	enum class PinPos { LowerLeft, UpperLeft, UpperRight, LowerRight, None };
	struct Impl; //メンバ変数の隠蔽
	std::shared_ptr<Impl> pImpl;

	bool formsRectangle();

public:
	int score;

	Field();
	~Field() = default;

	void update();
	void draw() const;

	bool isAttacking() const;
	bool isFullOfMonster() const;
	size_t getNumOfMonster() const;
	size_t getNumOfPins() const;
	bool select(const Point& pos);
	bool cancel();
	bool fire();
};