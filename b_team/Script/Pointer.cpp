#include "Pointer.h"


Pointer::Pointer(Field& field) 
	:field(field),
	pointingPos(4, 3),
	t(0)
{}


void Pointer::update() {
	
	if (++t / 5 >= 9) t = 0;

	//ごめんなさい
	if (Input::KeyW.clicked || Input::KeyA.clicked || Input::KeyS.clicked || Input::KeyD.clicked) SoundAsset(L"pin_move").playMulti();

	int tmp;
	pointingPos.x = (tmp = pointingPos.x + Input::KeyD.clicked - Input::KeyA.clicked) >= 0 ? tmp % colLen : colLen + tmp;
	pointingPos.y = (tmp = pointingPos.y + Input::KeyS.clicked - Input::KeyW.clicked) >= 0 ? tmp % rowLen : rowLen + tmp;

	if (Input::KeyJ.clicked) {
		field.select(pointingPos);
		if (field.getNumOfPins() == 4) field.fire();
	}
	else if (Input::KeyK.clicked) field.cancel();
}


void Pointer::draw() const {
	const Point pos(offset_x + pointingPos.x*cellSize, offset_y + pointingPos.y*cellSize);
	TextureAsset(L"pointer")(pointerSize*(t/5) ,0, pointerSize, pointerSize).drawAt(pos);
}