#include "Game.h"


CountDown::CountDown()
	:t(0),
	start(false)
{
}

void CountDown::update() {
	if (!start) {
		int pre = t / 60;
		if (++t / 60 >= 3) { start = true; t = 0; SoundAsset(L"start").play(); }
		else if (t / 60 != pre || t == 1) SoundAsset(L"cursor_move").play();
	}
	else {
		if (++t >= 90) { start = false; t = 0; kill(); }
	}
}

void CountDown::draw() const {
	if (!start) FontAsset(L"font")(3 - t / 60).drawAt(Window::Center(), Palette::Black);
	else FontAsset(L"font")(L"Start").drawAt(Window::Center(), Palette::Black);
}



Finish::Finish()
	:t(-FontAsset(L"font")(L"Finish").region().w/2),
	sound_flag(false)
{}

void Finish::update() {
	static const int textWidth = FontAsset(L"font")(L"Finish").region().w;
	t += 5;
	if (!sound_flag && t >= Window::Width() / 4) sound_flag = true, SoundAsset(L"finish").play();
	if (t >= Window::Width()+textWidth/2) kill();
}

void Finish::draw() const {
	FontAsset(L"font")(L"Finish").drawAt(t, Window::Center().y, Palette::Black);
}




Game::Game()
	:field(),
	pointer(field),
	eventManager(),
	func_update(),
	gameFont(15, L"coopoppo 3D")
{}

void Game::init()
{
	SoundAsset(L"BGM").setLoop(true);
	SoundAsset(L"finish").changeTempo(0.8);

	eventManager.registerEvent(new CountDown());
	
	func_update = [&] {
		if (eventManager.hasEvent()) eventManager.update();
		else {
			SoundAsset(L"BGM").play(1.0s);
			func_update = [&] {
				field.update(); pointer.update();
				m_data->score = field.score;
				if (field.isFullOfMonster()) {
					SoundAsset(L"BGM").stop();
					eventManager.registerEvent(new Finish());
					func_update = [&] {
						if (eventManager.hasEvent()) eventManager.update();
						else changeScene(L"Result");
					};
				}
			};
		}
	};
}

void Game::update()
{
	func_update();
}

void Game::draw() const
{
	field.draw();
	pointer.draw();

	//モンスターゲージ
	static const int gage_width = TextureAsset(L"DangerGage").width;
	static const int bar_width = TextureAsset(L"DangerBar").width;
	static const int bar_height = TextureAsset(L"DangerBar").height;
	const int space = 35;
	const int bar_offset = 40;
	const Point gagePos(Window::Width() - gage_width - space, 0);
	float num_of_monster = field.getNumOfMonster() > monster_capacity ? monster_capacity : field.getNumOfMonster();
	TextureAsset(L"DangerBar")(0, 0, bar_width*(num_of_monster/monster_capacity), bar_height).draw(gagePos.movedBy(bar_offset, 0));
	TextureAsset(L"DangerGage").draw(gagePos);

	//スコアの描画
	const Point scorePos(space, 0);
	gameFont(L"SCORE ", m_data->score).draw(scorePos);


	if (eventManager.hasEvent()) eventManager.draw();

}