#pragma once
#include "SceneBase.h"
#include "Pointer.h"
#include "EventSystem.h"


class CountDown : public ymds::Event {
private:
	int t;
	bool start;

public:
	CountDown();
	~CountDown() = default;

	void update() override;
	void draw() const override;

};


class Finish : public ymds::Event {
private:
	int t;
	bool sound_flag;

public:
	Finish();
	~Finish() = default;

	void update() override;
	void draw() const override;

};


class Game : public SceneManager<String, CommonData>::Scene
{
public:
	Game();

	void init() override;

	void update() override;

	void draw() const override;

private:
	Field field;
	Pointer pointer;
	ymds::EventManager eventManager;
	const Font gameFont;

	std::function<void(void)> func_update;
};

