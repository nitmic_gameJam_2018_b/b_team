# include "Monster.h"


Monster::Monster(int speed_, float hp_, String texture_name_, int pts_) 
	:count(0),
	m_status(static_cast<Status>(Random<int>(0,3))),
	pos(Random<int>(0, 8), Random<int>(0, 5)),
	drawPos((int)cellSize*pos),
	speed(speed_),
	hp(hp_),
	damaged(false),
	t(0),
	dead(false),
	texture_name(texture_name_),
	pts(pts_)
{
	move();
}

void Monster::changeStatus() {
	int r = Random<int>(0, 3);
	m_status = static_cast<Status>(r);
}

void Monster::move() {
	changeStatus();

	Point pre_pos(pos); //動く前の座標

	switch (m_status)
	{
	case Monster::Status::Up:
		pos.y--; break;
	case Monster::Status::Down:
		pos.y++; break;
	case Monster::Status::Right:
		pos.x++; break;
	case Monster::Status::Left:
		pos.x--; break;
	default: break;
	}

	//範囲外に出たら
	if (pos.x < 0 || pos.x > 8 || pos.y < 0 || pos.y > 5)  pos = pre_pos;
}

int Monster::damage(int damage) {
	damaged = true;
	t = 0;
	if ((hp -= damage) <= 0) {
		m_status = Status::Erase;
		return pts;
	}
	return 0;
}


void Monster::update() {

	if (damaged) {
		if (++t >= 30) damaged = false, t = 0;
	}
	else if (m_status == Status::Erase) {
		if (++t >= 45) dead = true, t = 0;
	}
	else {
		if ((++t / 8) >= 3) t = 0;

		if (drawPos != (int)cellSize*pos) {
			switch (m_status)
			{
			case Monster::Status::Up:
				drawPos.y -= speed; break;
			case Monster::Status::Down:
				drawPos.y += speed; break;
			case Monster::Status::Left:
				drawPos.x -= speed; break;
			case Monster::Status::Right:
				drawPos.x += speed; break;
			default: break;
			}
			if (drawPos == (int)cellSize*pos) move();
		}
		else move();
	}
}


void Monster::draw() const {
	if (damaged) TextureAsset(texture_name)(0, 4 * cellSize, 64, 64).draw(drawPos + Point(offset_x, offset_y));
	else TextureAsset(texture_name)((t / 8)*cellSize, static_cast<int>(m_status)*cellSize, 64, 64).draw(drawPos + Point(offset_x, offset_y), m_status == Status::Erase ? Palette::Red : Palette::White);

}