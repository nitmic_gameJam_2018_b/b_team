#pragma once
# include "Monster.h"

class Slime : public Monster
{
public:
	Slime();
	~Slime() = default;

};

class Wolf : public Monster
{
public:
	Wolf();
	~Wolf() = default;

};

class Golden : public Monster
{
public:
	Golden();
	~Golden() = default;

};

class Kinoko : public Monster
{
public:
	Kinoko();
	~Kinoko() = default;

};