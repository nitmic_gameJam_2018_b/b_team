# include "Field.h"


MonsterGenerator::MonsterGenerator(std::list<std::shared_ptr<Monster>>& list)
	:monsterList(list),
	interval(180),
	increase_coeff(3),
	t(0),
	count(0)
{}

void MonsterGenerator::generate() {
	if (++t >= interval) {
		if (++count > 10) increase_coeff *= 1.5, count = 0;
		rep(increase_coeff) {
			int r = Random<int>(0, 10);
			if (r <= 4) monsterList.emplace_back(new Slime());
			else if (r <= 7) monsterList.emplace_back(new Wolf());
			else if (r <= 9) monsterList.emplace_back(new Kinoko());
			else monsterList.emplace_back(new Golden());
		}
		t = 0;
	}
}




struct Field::Impl {
	std::array<std::array<bool, 10>, 7> points;
	std::list<std::shared_ptr<Monster>> monsterList;
	std::vector<std::pair<Point, PinPos>> pins;
	int t = 0; //描画用
	bool attacking = false;
	Point vartex[4];
	MonsterGenerator generator;

	Impl()
		:generator(monsterList)
	{}
};



Field::Field()
	:pImpl(new Impl()),
	score(0)
{
	for (auto& arr : pImpl->points) {
		for (auto& elm : arr) elm = false;
	}
}

void Field::update() {

	for (auto& monster : pImpl->monsterList) monster->update();

	if (!pImpl->attacking) {
		if (++pImpl->t / 4 >= 4) pImpl->t = 0;
	}
	else {
		if (++pImpl->t / 6 >= 4) {
			pImpl->t = 0;
			pImpl->attacking = false;
			pImpl->pins.clear();
		}
	}
	pImpl->generator.generate();
	pImpl->monsterList.remove_if([](std::shared_ptr<Monster>& elm) { return elm->isDead(); });
}

void Field::draw() const {

	auto& var_ = pImpl->vartex;
	TextureAsset(L"back").draw();
	
	for (const auto& monster : pImpl->monsterList) monster->draw();

	if (!pImpl->attacking) {
		for (const auto& elm : pImpl->pins) {
			const Point pos(offset_x + elm.first.x*cellSize - pointerSize / 2, offset_y + elm.first.y*cellSize - pointerSize);
			TextureAsset(L"pin")((pImpl->t / 4)*pointerSize, 0, pointerSize, pointerSize).draw(pos);
		}
	}
	else {
		size_t alpha = 100 + 155 / ((var_[1].x - var_[0].x)*(var_[2].y - var_[0].y));
		Rect(offset_x + var_[0].x*cellSize, offset_y + var_[0].y*cellSize,
			(var_[1].x - var_[0].x)*cellSize, (var_[2].y - var_[0].y)*cellSize).draw(Color(255,0,0,alpha));


		for (const auto& elm : pImpl->pins) {
			const Point pos(offset_x + elm.first.x*cellSize - pointerSize / 2, offset_y + elm.first.y*cellSize - pointerSize);
			TextureAsset(L"pin")((pImpl->t / 6)*pointerSize, (static_cast<int>(elm.second) + 1)*pointerSize, pointerSize, pointerSize).draw(pos);
		}
		rep(2 * (var_[1].x - var_[0].x) - 1) {
			const Point posL(offset_x + var_[0].x*cellSize + pointerSize / 2 + i*pointerSize, offset_y + var_[0].y*cellSize - pointerSize);
			const Point posU(posL.movedBy(0, cellSize*(var_[2].y - var_[0].y)));
			auto&& texture = TextureAsset(L"Laser")((pImpl->t / 6)*pointerSize, pointerSize, pointerSize, pointerSize);
			texture.draw(posL);
			texture.draw(posU);
		}
		rep(2 * (var_[2].y - var_[0].y) - 1) {
			const Point posL(offset_x + var_[0].x*cellSize - pointerSize / 2, offset_y + var_[0].y*cellSize + i*pointerSize);
			const Point posR(posL.movedBy(cellSize*(var_[1].x - var_[0].x), 0));
			auto&& texture = TextureAsset(L"Laser")((pImpl->t / 6)*pointerSize, 0, pointerSize, pointerSize);
			texture.draw(posL);
			texture.draw(posR);
		}
	}
}

bool Field::select(const Point& pos) {
	if (pImpl->pins.size() >= num_of_selectPin) return false;
	for (const auto& pin : pImpl->pins) {
		if (pin.first == pos) return false;
	}SoundAsset(L"pin").playMulti();
	pImpl->pins.emplace_back(pos, PinPos::None);
	return true;
}

bool Field::cancel() {
	if (pImpl->pins.size() <= 0) return false;
	SoundAsset(L"cancel").playMulti();
	pImpl->pins.erase(--pImpl->pins.end());
	return true;
}

//くそ雑な実装でごめんなさい
bool Field::formsRectangle() {

	if (pImpl->pins.size() != 4) return false;

	auto& pins = pImpl->pins;

	//左上の点を決める
	std::pair<Point,PinPos>* tmp = &pins[0];
	for (auto& elm : pins) {
		auto t = elm.first.x + elm.first.y;
		if (t < tmp->first.x + tmp->first.y) tmp = &elm;
	}
	pImpl->vartex[0] = tmp->first;
	tmp->second = PinPos::UpperLeft;

	//右上、左下を決める
	for (auto& elm : pins) {
		if (elm.first.y == pImpl->vartex[0].y && elm.first.x != pImpl->vartex[0].x) pImpl->vartex[1] = elm.first, elm.second = PinPos::UpperRight;
		else if (elm.first.x == pImpl->vartex[0].x && elm.first.y != pImpl->vartex[0].y) pImpl->vartex[2] = elm.first, elm.second = PinPos::LowerLeft;
		else if (elm.first.x != pImpl->vartex[0].x && elm.first.y != pImpl->vartex[0].y) pImpl->vartex[3] = elm.first, elm.second = PinPos::LowerRight;
	}
	if (pImpl->vartex[1].x > 10 || pImpl->vartex[2].x > 10) return false;
	if (pImpl->vartex[3].x == pImpl->vartex[1].x && pImpl->vartex[3].y == pImpl->vartex[2].y) return true;

	return false;
}

bool Field::fire() {

	//vartexのリフレッシュ
	rep(4) pImpl->vartex[i].set(11, 11);

	if (!formsRectangle()) return false;

	auto& var_ = pImpl->vartex;
	float damage = damageCoeff / ((var_[1].x - var_[0].x)*(var_[2].y - var_[0].y));

	pImpl->attacking = true;
	SoundAsset(L"surround").play();
	for (auto& elm : pImpl->monsterList) {
		Point pos(elm->drawPos.x + cellSize / 2, elm->drawPos.y + cellSize / 2);
		if (var_[0].x*cellSize <= pos.x && pos.x < var_[1].x*cellSize && var_[0].y*cellSize <= pos.y && pos.y < var_[2].y*cellSize) {
			score += elm->damage(damage);
		}
	}

	return true;

}



bool Field::isFullOfMonster() const { return pImpl->monsterList.size() > monster_capacity; }

bool Field::isAttacking() const { return pImpl->attacking; }

size_t Field::getNumOfMonster() const { return pImpl->monsterList.size(); }

size_t Field::getNumOfPins() const { return pImpl->pins.size(); }