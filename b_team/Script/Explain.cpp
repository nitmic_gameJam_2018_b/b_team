#include "Explain.h"

void Explain::init()
{

}

void Explain::update()
{
	if (Input::KeyK.clicked&&page == 2) changeScene(L"Title");
	if (Input::KeyK.clicked&&page != 2) page++;
	if (Input::KeyJ.clicked && page != 0) page--;
}

void Explain::draw() const
{
	TextureAsset(L"back").draw();
	Window::ClientRect().draw(Color(255,255,255,125));
	switch (page) {
	case 0 :EfontB(L"操作方法").draw(30, 25, Palette::Black);
			Efont(L"WASDでポインターが\n↑←↓→に動きます。\nJでピンを設置します。\nKでひとつ前のピンを\n消すことができます。").draw(80, 100, Palette::Black);
			EfontS(L"Kで進む→").draw(465, 412, Palette::Black);
			 break;
	case 1 :EfontB(L"ルール").draw(30, 25, Palette::Black);
			Efont(L"4つのピンで敵を囲うと\n攻撃できます。\nフィールドに敵が\n増えすぎると\nゲームオーバーです。").draw(80, 100, Palette::Black);
			EfontS(L"←Jで戻る").draw(35, 412, Palette::Black);
			EfontS(L"Kで進む→").draw(465, 412, Palette::Black);
			break;
	case 2:	EfontB(L"ルール").draw(30, 25, Palette::Black);
			EfontS(L"←Jで戻る").draw(35, 412, Palette::Black);
			Efont(L"囲う面積が小さいほど\n敵に与えるダメージが\n大きくなります。").draw(80, 100, Palette::Black);
			EfontS(L"Kでタイトルに戻る→").draw(315, 412, Palette::Black); break;
	}
	
	
}