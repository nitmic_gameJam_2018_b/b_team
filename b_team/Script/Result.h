#pragma once
#include "SceneBase.h"

class Result : public SceneManager<String, CommonData>::Scene
{
public:

	void init() override;

	void update() override;

	void draw() const override;

private:
	int t = 0;
	bool drumming = true;
	bool drum_fin = false;
	float score_ = 0;
	float delta;
	Font gameFont;
	Font font;

};

