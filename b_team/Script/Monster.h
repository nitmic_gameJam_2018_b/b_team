#pragma once
# include "Base.h"


class Monster{
protected:
	Point pos; //(0 <= x <= 9), (0 <= y <= 6)

public:
	Point drawPos; //描画用のポイント
	const int pts;

protected:
	enum class Status {
		Up,
		Down,
		Left,
		Right,
		Erase
	} status;
	Status m_status;
	int speed;
	float hp;
	bool damaged;
	bool dead;
	int t; //描画用
	const String texture_name;

	virtual void move();

private:
	int count;
	void changeStatus();

public:
	Monster(int speed, float hp_, String texture_name, int pts);
	virtual ~Monster() = default;
	void update();
	void draw() const;
	bool isDead() { return dead; }
	int damage(int damage);
	
};