#pragma once
#include "SceneBase.h"

class Title : public SceneManager<String, CommonData>::Scene
{
public:
	Title()
		:selection(Selection::START),
		TFont(17),
		selectedColor(Palette::Red),
		cursorPos(215, 295)
	{}

	void init() override;

	void update() override;

	void draw() const override;

private:
	Font TFont;
	enum class Selection { START, EXPLAIN, EXIT } selection;
	Color selectedColor;
	Point cursorPos;
};

