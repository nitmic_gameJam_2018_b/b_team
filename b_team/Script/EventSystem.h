﻿#pragma once
# include <list>
# include <algorithm>


//EventManagerクラスを作って、Eventクラスを継承した処理のクラスを登録すればよい

namespace ymds {

	using namespace std;
	
	//killメソッドを提供するインターフェース
	class EventKiller {
	protected:
		bool dead;

	public:
		EventKiller() : dead(false) {}
		virtual void kill() = 0;

	};


	//具象クラスではupdateメソッドとdrawメソッド、(必要ならば)デストラクタを実装
	//update内の任意のタイミングでkillメソッドを呼ぶ
	class Event {
	private:
		EventKiller* manager_ptr;

	protected:
		void kill() { manager_ptr->kill(); }

	public:
		virtual ~Event() {}
		virtual void update() = 0;
		virtual void draw() const = 0;
		void setManager(EventKiller* ptr) { manager_ptr = ptr; }

	};


	//Eventクラスのラッパークラス
	/* 抽象クラスをlistに入れることができないので(ポインタなら可能だが、扱いが複雑になる),ラッパークラスを作った */
	class EventOwner final : public EventKiller {
	private:
		Event* event;
		bool* shouldErase_ref;

	public:
		EventOwner(Event* arg1, bool* arg2)
			:EventKiller(),
			event(arg1),
			shouldErase_ref(arg2)
		{
			arg1->setManager(this);
		}

		~EventOwner() { delete event; }

		void execute() { event->update(); }
		void draw() const { event->draw(); }
		bool isDead() { return dead; }
		void kill() override { dead = true; *shouldErase_ref = true; }

	};


	//Eventの管理クラス
	class EventManager final {
	private:
		list<EventOwner> eventList;
		bool shouldErase;

	public:
		EventManager() : shouldErase(false) {}

		void update() {
			for (auto& elm : eventList) elm.execute();

			if (shouldErase) {
				eventList.remove_if([](EventOwner& elm) { return elm.isDead(); }); //STLのremoveとは違い、ちゃんと削除している
				shouldErase = false;
			}
		}
		void draw() const{
			for (const auto& elm : eventList) elm.draw();
		}

		void registerEvent(Event* event) { eventList.emplace_back(event, &shouldErase); }
		list<EventOwner>& getList() { return eventList; }
		bool hasEvent() const { return eventList.size() > 0; }
	};

}
