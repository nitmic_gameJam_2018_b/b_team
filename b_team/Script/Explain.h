#pragma once
#include "SceneBase.h"

class Explain : public SceneManager<String, CommonData>::Scene
{
public:
	Explain()
		:page(0),
		Efont(30),
		EfontB(40),
		EfontS(20)
	{}

	void init() override;

	void update() override;

	void draw() const override;

private:
	int page;
	Font Efont;
	Font EfontB;
	Font EfontS;
};

