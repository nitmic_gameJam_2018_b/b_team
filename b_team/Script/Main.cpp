﻿# include "Title.h"
# include "Explain.h"
# include "Game.h"
# include "Result.h"
# include "Fileregister.h"


void Main()
{
	AssetRegisterer().fileRegister(L"Files", true).preload();
	FontManager::Register(L"Files/coopoppo_3d.ttf");

	SceneManager<String, CommonData> manager;

	manager.add<Title>(L"Title");
	manager.add<Explain>(L"Explain");
	manager.add<Game>(L"Game");
	manager.add<Result>(L"Result");
	
	while (System::Update())
	{
		manager.updateAndDraw();
	}
}
