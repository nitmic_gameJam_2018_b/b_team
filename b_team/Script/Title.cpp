#include "Title.h"

void Title::init()
{
	if (!SoundAsset(L"BGM").isPlaying()) {
		SoundAsset(L"BGM").setLoop(true);
		SoundAsset(L"BGM").play(0.5s);
	}
	m_data->score = 0;
}

void Title::update()
{
	//カーソル移動
	int now = static_cast<int>(selection);
	selection = static_cast<Selection>((now + Input::KeyS.clicked - Input::KeyW.clicked + 3) % 3);

	if (Input::KeyW.clicked || Input::KeyS.clicked) SoundAsset(L"cursor_move").playMulti(0.8);

	if (Input::KeyJ.clicked) {
		SoundAsset(L"decision").play();
		switch (selection)
		{
		case Title::Selection::START:
			SoundAsset(L"BGM").stop();
			changeScene(L"Game");
			break;
		case Title::Selection::EXPLAIN:
			changeScene(L"Explain");
			break;
		case Title::Selection::EXIT:
			System::Exit();
			break;
		default:
			break;
		}
	}

}

void Title::draw() const
{
	TextureAsset(L"Title").draw();
	TextureAsset(L"TitlePointer").draw(cursorPos.movedBy(-25*(static_cast<int>(selection)%2), 41*static_cast<int>(selection)));
	TFont(L"WSキーで移動、Jキーで決定").drawAt(Window::Center().movedBy(0, Window::Height()/2 - 35), Palette::Black);
}